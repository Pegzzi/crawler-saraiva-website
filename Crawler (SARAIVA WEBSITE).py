# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import requests

from bs4 import BeautifulSoup

def get_http(url, nome_livro):
    nome_livro=nome_livro.replace(' ','+')
    url = '{0}?q={1}&sort=13'.format(url,nome_livro)
       # http://pesquisa.saraiva.com.br/busca?q=jogos+ps3&sort=13
    try:
        return requests.get(url)
    except (requests.exceptions.HTTPError) as e:
        print(str(e))    
        pass
    except Exception as e:
        raise
      
        
        
def get_produtos(content):
    soup = BeautifulSoup(content, 'html.parser')
    produtos=soup.find_all('div',{'class':'nm-product-info'})
    
    lista_produtos=[]

    for element in produtos:
        info_produtos=[element.a.get('href'), element.a.get('title'), element.find('div', attrs={'class':'nm-price-value'}).string]
        lista_produtos.append(info_produtos)
    
    
    return lista_produtos
    
def get_http_page_produto(lista_produtos):
    
    d={}
    lista_prod=[]
    
    for produto in lista_produtos:
        
        try:
            r=requests.get('http:'+produto[0])
        except (requests.exceptions.HTTPError, requests.exceptions.RequestException) as e:
            print(str(e))
            r=None
        except Exception as e:
            raise
        
        d = parse_page_produto(r.text, produto[0],produto[1])
        lista_prod.append(d.copy())
        
        return lista_prod
    
def parse_page_produto(content, url_produto, titulo):
    soup=BeautifulSoup(content,'lxml')

    preco=soup.find('span',{'class':'final-price no-extras'})
    
    d={}
    try:
        preco=preco.string
        d={
           'titulo':titulo,
           'Preço':preco,
           'url_produto':url_produto
        }
    except AttributeError as e:
        pass
    return d
    
    
    
if __name__=='__main__':
    
    url ='http://pesquisa.saraiva.com.br/busca'
    nome_livro = ('jogo ps3')
    
    r =get_http(url,nome_livro)
    
    #with open('result.html','w', encoding='utf-8') as f:
    #   f.write(r.text)
        
    if r:
        lista_produtos=get_produtos(r.text)
        lista = get_http_page_produto(lista_produtos)
        print(lista)
        
    for element in lista:
        print(element)
           
